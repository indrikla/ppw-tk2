from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse, resolve
from django.http import HttpRequest
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class LoginUnitTest(TestCase):
    def test_if_url_exsit(self):
        response = Client().get("/login")
        self.assertEquals(response.status_code, 200)

    def test_template_used(self):
        response = Client().get("/login")
        self.assertTemplateUsed(response, "login.html")

    def test_register_url_exist_view_template_used(self):
        response = Client().get("/signup")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "registration.html")
    
    def test_logout(self):
        response = Client().get("/logout")
        self.assertRedirects(response, "/")
