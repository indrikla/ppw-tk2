from django.urls import path
from .views import activity, data

app_name = 'productivity'

urlpatterns = [
    path('productivity-tracker/', activity, name='activity'),
    path('productivity-tracker/activity-data', data, name='activity_data'),
]