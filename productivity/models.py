from django.db import models

class Activity(models.Model):
    activity = models.CharField(max_length=100)
    is_checked = models.BooleanField(default=False)