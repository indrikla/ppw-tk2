from django.shortcuts import render, redirect
from .models import Activity
from .forms import ActivityForm
from django.contrib import messages

import requests
import json
from django.core import serializers
from django.http import JsonResponse

def activity(request):
    if request.method == 'POST':
        if request.is_ajax():
            form = ActivityForm(request.POST)
            if form.is_valid():
                activity_obj = Activity()
                activity_obj.activity = form.cleaned_data['activity']
                activity_obj.save()
                return JsonResponse({'success':True})
        return JsonResponse({'success':False})
    else:
        form = ActivityForm()
        return render(request, 'tracker.html', {'form':form})

def data(request):
    json_data = serializers.serialize('json', Activity.objects.all())
    return JsonResponse(json_data, safe=False)