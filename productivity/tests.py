from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity
from .views import activity, data
from .apps import ProductivityConfig

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("productivity:activity")
        self.test_activity = Activity.objects.create(
            activity = "belajar"
        )

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "tracker.html")

    def test_konten(self):
        response = self.client.get(self.activity_url)
        self.assertContains(response, "to-do")
        self.assertContains(response, "feel better")
        self.assertContains(response, "healing")
        self.assertContains(response, "sharing")
        self.assertContains(response, "wonderful day")

    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve("/productivity-tracker/")
        self.assertEqual(found.func, activity)
    
    def test_apakah_mengakses_function_page_json_yang_benar(self):
        found = resolve("/productivity-tracker/activity-data")
        self.assertEqual(found.func, data)

    def test_get_activity(self):
        Activity.objects.create(activity='tidur')
        response = self.client.get('/productivity-tracker/activity-data')
        self.assertEqual(200, response.status_code)
    