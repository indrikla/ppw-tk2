from django.db import models
from django.forms.fields import JSONField
from django.contrib.auth.models import User

# Create your models here.
class Quote(models.Model):
    quote = models.TextField('Quote', max_length=200, null=True, blank=False)
    # info = JSONField(null=True, blank=True)

    def __str__(self):
        return f'{self.quote}'

    class Meta:
        db_table = 'quote'

class QuoteRec(models.Model):
    quoteRec = models.TextField('Quote', max_length=200, null=True, blank=False)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE,null=True)

    def __str__(self):
        return f'{self.quoteRec}'

    class Meta:
        db_table = 'quoteRec'