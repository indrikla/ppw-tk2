from django.shortcuts import render
from .models import Quote
from django.db.models import Max
import random
import requests
from django.http import JsonResponse
from django.db.models.fields import json
from quotesgen.models import QuoteRec
from django.core import serializers


# Create your views here.
def quotesgen(request):
    return render(request, 'quotes_gen.html')

def getQuotes(request):
    # max_id = Quote.objects.all().aggregate(max_id=Max("id"))['max_id']
    # pk = random.randint(1, max_id)
    # result = Quote.objects.get(pk=pk)
    # response_json = result.json()
    json_data = serializers.serialize('json', Quote.objects.all())
    print(json_data)
    return JsonResponse(json_data, safe=False)
    
def postQuotes(request):
    data = {'success': False} 
    if request.method == 'POST':
        qdb = QuoteRec()
        qdb.quoteRec = request.POST.get('quoteRec')
        qdb.user_id = request.user
        qdb.save()
        data['success'] = True
    return JsonResponse(data, safe=False)