from django.urls import path
from . import views

urlpatterns = [
    path('', views.quotesgen, name = 'quotesgen'),
    path('postQuotes', views.postQuotes, name='postQuotes'),
    path('getQuotes', views.getQuotes, name='getQuotes')
]