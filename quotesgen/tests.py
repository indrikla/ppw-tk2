from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import *
from .views import *

# Create your tests here.
class QuotesGenUnitTest(TestCase):
    def test_activity_url_data_is_exist(self):
        response = Client().get("/quotes/")
        response2 = Client().get("/quotes/getQuotes")
        response3 = Client().get("/quotes/postQuotes")
        self.assertEquals(200, response.status_code)
        self.assertEquals(200, response2.status_code)
        self.assertEquals(200, response3.status_code)
    
    def test_template_form(self):
        response = Client().get("/quotes/")
        self.assertTemplateUsed(response, "quotes_gen.html")

    def test_model_create(self):
        contoh = Quote.objects.create(quote="if you think you can you can")
        jumlah = Quote.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_model_str(self):
        contoh = Quote.objects.create(quote="restyour")
        self.assertEqual("restyour", str(contoh))

    def test_if_user_not_logged_in(self):
        response = Client().get("/quotes/")
        isi_html = response.content.decode("utf8")
        self.assertIn('LogIn</a></span> to', isi_html)

