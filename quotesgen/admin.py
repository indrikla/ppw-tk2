from django.contrib import admin
from .models import *

# Register your models here.

@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'quote')

@admin.register(QuoteRec)
class QuoteRecAdmin(admin.ModelAdmin):
    list_display = ('id', 'quoteRec', 'user_id')




