from django.forms import ModelForm
from .models import Name
from django import forms


class NameForm(forms.ModelForm):
    #    name = forms.CharField(label='name', max_length=30, required=True)
    name = forms.CharField(widget=forms.Textarea, label='', required=True)

    class Meta:
        model = Name
        fields = ['name']


class MoodForm(forms.Form):
    mood = forms.CharField(label="", widget=forms.TextInput(attrs={
        'name': 'mood',
        'class': 'form-control',
        'placeholder': 'How are you feeling now?',
        'type': 'text',
        'style': 'width: 30vw;',
        'required': True
    }))
