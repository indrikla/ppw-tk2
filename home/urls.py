from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.landingPage, name='landingPage'),
    path('home/', views.home, name='home'),
    path('homeAfterLogin/', views.homeAfterLogin, name='homeAfterLogin'),
    path('movies/', views.movies, name='movies'),
    # path('movies/api/movies/<str:movie>/', views.retrieve, name='retrieve'),
    path('movies/data/', views.data, name='data'),
]
