from django.shortcuts import render, redirect
from .models import Name
from .forms import NameForm, MoodForm
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib import messages
import json
import requests
# Create your views here.


def home(request):
    save = "Someone"
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            save = form.save()
    context = {
        'name': save
    }
    return render(request, 'home.html', context)


def homeAfterLogin(request):
    return render(request, 'homeAfterLogin.html')


def landingPage(request):
    form = NameForm()
    context = {
        'form': form,
    }
    return render(request, 'landingPage.html', context)


def movies(request):
    if request.method == 'POST':
        form = MoodForm()
        if form.is_valid():
            form.save()
            return redirect('home:movies')
    else:
        form = MoodForm()

    return render(request, 'movies.html', {'form': form})


# def retrieve(request, movie):
#     argReq = requests.get(
#         "https://api.themoviedb.org/3/tv/popular?api_key=b70f627824fe3608d10d7e5a0b53b517&language=en-US&page=1" + movie)
#     data = argReq.json()
#     data = json.dumps(data)
#     # print(data)
#     return JsonResponse(content=data, content_type="application/json")
# test yang bawah
# test lagi
# test doang

def data(request):
    try:
        q = request.GET['q']

    except:
        q = 'quilting'

    json_read = requests.get(
        'https://api.themoviedb.org/3/tv/popular?api_key=b70f627824fe3608d10d7e5a0b53b517&language=en-US&page=1' + q).json()
    return JsonResponse(json_read)
