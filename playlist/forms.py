from django.forms import ModelForm
from .models import Comment
from django import forms


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['name','comment']

        widgets = {
            'name': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Your name here.',
                'required':True,}),
            'comment': forms.Textarea(attrs={
                'class':'form-control',
                'placeholder':'Your comment here.',
                'required':True}),
        }
