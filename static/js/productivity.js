$(document).ready(()=>{

    $('span').hover( function(){
        $(this).css("color", "#253031");
    }, function(){
        $(this).css("color", "#8BF59C");
    })

    $('#toDo_button').click(function(){
        let activity = $("#id_activity").val();

        if(activity != ""){
            console.log("aaaaaaaaaaaaaaaaaaa")
            $.ajax({
                method: 'POST',
                url: '/productivity-tracker/',
                datatype:'json',
                data: $("#form").serialize(),

                success: function(data) {
                    if (data['success']) {
                        $('#id_activity').val('')
                        alert("Successfully added new activity!")
                        update_activity();
                    }
                }
            }) 
        } 
    })

    $(document).ready(function(){
        $.ajax({
            method: 'GET',
            url: '/productivity-tracker/activity-data',
            datatype:'json',
            success: function(data) {
                console.log(data);
                $('#activity-container').html('')
                var result = '';
                var JSONData = JSON.parse(data);
                for (var i = 0; i < JSONData.length; i++){                            
                    var obj = JSONData[i];
                    console.log(obj);
                    console.log(obj.fields.activity);
                    var listnya = obj.fields.activity;
                    result +=       
                    "<div class='flex-container' id='list_activity'>" +
                        "<div class='btn-group-toggle' data-toggle='buttons'>" +
                            "<label class='btn checkmark active'><input type='checkbox'><span class ='checkmark'></span></label>" +
                        "</div>" +
                        "<div class='card list'>" +
                            "<h5 class='activity col-10' id='list'>" + listnya  + "</h5>" +
                        "</div>" +
                    "</div>"
                }
                $('#activity-container').append(result);
            }
        })
    })

    function update_activity(){
        $.ajax({
            method: 'GET',
            url: '/productivity-tracker/activity-data',
            datatype:'json',
            success: function(data) {
                console.log(data);
                $('#activity-container').html('')
                var result = '';
                var JSONData = JSON.parse(data);
                for (var i = 0; i < JSONData.length; i++){                            
                    var obj = JSONData[i];
                    console.log(obj);
                    console.log(obj.fields.activity);
                    var listnya = obj.fields.activity;
                    result +=       
                    "<div class='flex-container' id='list_activity'>" +
                        "<div class='btn-group-toggle' data-toggle='buttons'>" +
                            "<label class='btn checkmark active'><input type='checkbox'><span class ='checkmark'></span></label>" +
                        "</div>" +
                        "<div class='card list'>" +
                            "<h5 class='activity col-10' id='list'>" + listnya  + "</h5>" +
                        "</div>" +
                    "</div>"
                }
                $('#activity-container').append(result);
            }
        })
    }
})
